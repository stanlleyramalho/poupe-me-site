app.factory("servicesPesquisa", ['$http', function($http) {
    var serviceBase = 'http://vps4856.publiccloud.com.br:8000/api/';
    var obj = {};
/*
    obj.pesquisa = function(parametro){
        return $http.get(serviceBase + 'get/produtos/estabelecimento/'+parametro+'/-9.7527683/-36.6579829/15');
    };
    */
    obj.pesquisa = function(parametro){
        var json = {
              "usuario": "1",
              "tipo": "pesquisa",
              "parametro": parametro,
              "lat": "-9.75164",
              "long": "-36.6604",
              "distancia": "10"
            };

        console.log(JSON.stringify(json));

        return $http({
            method: 'post',
            url: serviceBase + 'produtos/',
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: "json",            
            headers : {
                'Content-Type': 'application/json'
            }
           });   
        /*return $http.post(serviceBase + 'produtos/',json);*/
    };       


    return obj;
}]);

app.controller('PesquisaCtrl', ['$scope','$routeParams', 'servicesPesquisa',
	function($scope, $routeParams, servicesPesquisa) {
	
	$scope.parametro = $routeParams.parametro;
	console.log($scope.parametro);


	servicesPesquisa.pesquisa($scope.parametro).then(function(data){
		console.log(data.data);
	    console.log(data.data.produtos);

	    $scope.produtos = data.data.produtos;
		
	});  

 
}]);