var app = angular.module('app',['ngRoute']);

app.config(function($routeProvider, $locationProvider)
{

   $routeProvider

   .when('/', {
      templateUrl : 'app/views/home.html',
      controller     : 'HomeCtrl',
   })

   .when('/pesquisa/:parametro', {
      templateUrl : 'app/views/pesquisa.html',
      controller  : 'PesquisaCtrl',
   })

    .when('/login', {
      templateUrl : 'app/views/login.html',
      controller  : 'LoginCtrl',
   })

   .otherwise ({ redirectTo: '/' });
});