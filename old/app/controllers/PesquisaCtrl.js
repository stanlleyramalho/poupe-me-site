app.factory("servicesPesquisa", ['$http', function($http) {
    var serviceBase = 'http://vps4856.publiccloud.com.br:8000/api/';
    var obj = {};

    obj.pesquisa = function(parametro){
        return $http.get(serviceBase + 'get/produtos/estabelecimento/'+parametro+'/-9.7527683/-36.6579829/15');
    };


    return obj;
}]);

app.controller('PesquisaCtrl', ['$scope','$routeParams', 'servicesPesquisa',
	function($scope, $routeParams, servicesPesquisa) {
	
	$scope.parametro = $routeParams.parametro;
	console.log($scope.parametro);


	servicesPesquisa.pesquisa($scope.parametro).then(function(data){
	    console.log(data.data.produtos);

	    $scope.produtos = data.data.produtos;
		
	});  

 
}]);